Modular js principles (modlr?):

- page consists of independent html blocks - "html widgets"
- widgets may have css, images and js code associated with them
- js code for some class of the widgets (html blocks having the same structure) is called "js module"

- even if all the js modules are loaded at the page - init routines are invoked only for ones having corresponding html widgets at the page
- each module defines its dependencies on other modules and non-modular libraries, like js and its plugins (note - they may have css and images as well)
- if some of the dependencies are absent - developer should be informed ASAP
- js files which aren't needed - not loaded and developer have some way to find them out

- js module should have an ability to store data related to every html widget

- js module can attach event listeners to html widget, de-attach them and reload the widget with ajax if that's possible for the widget class

- js modules may trigger some events and react upon events triggered by other js modules, but they still should be decoupled using event manager. it should be easy to list the events module may trigger and react upon

- some debug mode should be available which logs events triggered

- prevents circular dependencies in events

- could we have some css / js inheritance across modules and widgets?

- killer feature in debug module: if some js module has changed - detect that and reload it. If some html module which supports reloading over ajax has changed - reload it (and try to restore its state)

TODO: 
- module should be aware of jQuery. We can even attach jQuery to module.jQuery or module.$ (vs windget.$)
- move some code from ModuleRepository to Module (e.g. widget handling)