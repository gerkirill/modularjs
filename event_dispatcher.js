var EventDispatcher = function(Repository) {

	var _callbacks = Repository();
	var _eventStack = [];
	const _DEFER = 'defer';

	function _eventTagsToStr(eventTags) {
		var result = '';
		for (var tag in eventTags) {
			result += (tag + ':' + eventTags[tag]+';');
		}
		return result;
	}

	function _alreadyInStack(eventTags) {
		var eventTagsStr = _eventTagsToStr(eventTags);
		return _eventStack.indexOf(eventTagsStr) != -1;
	}

	function _addToEventStack(eventTags) {
		var eventTagsStr = _eventTagsToStr(eventTags);
		_eventStack.push(eventTagsStr);
	}

	/*
	function _createDeferedCallbackNotificator(deferedCallbacksCounter, afterAllCallbacksExecutedCallback) {
		return function() {
			deferedCallbacksCounter--;
			if (deferedCallbacksCounter < 0) {
				throw "Event dispatcher exception: too much notifocations from defered callbacks";
			}
			if (deferedCallbacksCounter === 0) {
				afterAllCallbacksExecutedCallback();
			}
		};
	}
	*/
	return {

		DEFER: _DEFER,

		/**
		 * Subscribe to events matching "tags"
		 */
		subscribe: function(tags, callback) {
			_callbacks.add(callback, tags);
		},
		unsubscribe: function(tags) {
			var matchingCallbacks = _callbacks.find(tags);
			for (var i=0; i<matchingCallbacks.length; i++) {
				_callbacks.remove(matchingCallbacks[i]);
			}
		},
		/**
		 * Trigger event with related data stored into 'event' variable.
		 * This will execute registered callbacks matching 'tags'. Once all subscribed callbacks executed - 
		 * 'callback' will be called (if supplied)
		 */
		trigger: function(event, tags, callback) {
			var deferedCallbacksCount = 0;

			// avoid infinite reqursion when one event rizes another and vice versa
			if (_alreadyInStack(tags)) {
				return;
			}
			_addToEventStack(tags);

			if (typeof callback === 'undefined') {
				callback = function(){};
			}
			// notificator for async. event callbacks
			
			var callThisWhenDone = function() {
				deferedCallbacksCount--;
				if (deferedCallbacksCount < 0) {
					throw "Event dispatcher exception: too much notifocations from defered callbacks";
				}
				if (deferedCallbacksCount === 0) {
					callback();
				}
			}
			
			// notificator for async. event callbacks
			//var callThisWhenDone = _createDeferedCallbackNotificator(deferedCallbacksCount, callback);
			var matchingCallbacks = _callbacks.find(tags);
			for (var i=0; i<matchingCallbacks.length; i++) {
				var callbackResult = matchingCallbacks[i](event, callThisWhenDone);
				if (callbackResult === _DEFER) {
					// heck, event callback wants to do smth. in async. way! Not a problem - for this it should just
					// return eventDispatcher.DEFER, and call function supplied in 'callThisWhenDone' parameter when done.
					deferedCallbacksCount++;
				}
			}
			if (deferedCallbacksCount === 0) {
				callback();
			}
			_eventStack.pop();
		}
	};
};