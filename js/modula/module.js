define(["modula/repository"], function(Repository){
	var Module = function () {
		// TODO: should it have ablility to store some per-widget data except global state?
		var _widgets = Repository();
		var _identity;
		var _eventDispatcher;
		var _definition = {
			init: function(widget){},
			uninit: function(widget){},
			loadOnEvent: function(event){return false;},
			listeners: {}
		}

		function _initWidget(widget) {
			_definition.init(widget);
		}

		function _uninitWidget(widget) {
			_definition.uninit(widget);
		}

		function _getEventTags(eventName) {
			var eventTags = {'module': null, 'event': null, 'subscriber': _identity};
			var eventNameParts = name.split('.', 2);
			if (typeof eventNameParts[1] === 'undefined') {
				eventTags['event'] = eventName;
			} else {
				eventTags['event'] = eventNameParts[1];
				eventTags['module'] = eventNameParts[0];
			}
			return eventTags;
		}

		function _addWidget(widget) {
			_initWidget(widget);
			_widgets.add(widget, {});
		}

		function _removeWidget(widget) {
			_uninitWidget(widget);
			_widgets.remove(widget);
		}

		// passed to the module definator and thus available in init() function and event-listeners.
		// allows to: 1. trigger events; 2. add new widget from event listener code 3. remove widget from event listener code
		function _getModuleEndpoint() {
			return {
				trigger: function(eventName, eventData) {
					var tags = {/*module: _identity,*/ event: eventName};
					_eventDispatcher.trigger(eventData, tags);
				},
				addWidget: _addWidget,
				removeWidget: _removeWidget
			};
		}

		// if module is subscribed to some event and event is triggered - this function is used to notify
		// all the widgets module aware of about this event
		function _widgetEventNotificator(event, eventCallback) {
			var widgets = _widgets.getAll();
			for (var i=0; i<widgets.length; i++) {
				var widget = widgets[i];
				eventCallback(event, widget);
			}
		}

		return {
			// method used from the module js files to define module behavior - events binding to widget html elements,
			// trigger events and react on events from other modules. Module endpoint will be passed to moduleDefinator
			// function as a single param.
			define: function(identity, moduleDefinator) {
				_identity = identity;
				var definition = moduleDefinator(_getModuleEndpoint());
				for (key in definition) {
					_definition[key] = definition[key];
				}
			},
			// adds one more html widget module is aware of and binds events to its html
			addWidget: _addWidget,
			/*
			loadWidget: function() {

			},
			*/
			// used by module repository - to inject event dispatcher (common for all modules)
			setEventDispatcher: function(eventDispatcher) {
				_eventDispatcher = eventDispatcher;
			},
			// required by the module repository - to store into repository by identity
			getIdentity: function() {
				return _identity;
			},
			// required by the module repository, after module is defined - this will register its listeners.
			// event dispatcher should be injected before that
			registerEventListeners: function() {
				for (eventName in _definition.listeners) {
					var eventTags = _getEventTags(eventName);
					var eventCallback = _definition.listeners[eventName];
					// callback should receive 2 parameters - 1:event, 2:widget to act on.
					// event comes from eventDispatcher. widget is to be added by thre module
					_eventDispatcher.subscribe(eventTags, function(event){
						_widgetEventNotificator(event, eventCallback);
					});
				}
			},
			// when the module is removed from module repository - repo invokes this function to unregister
			// its listeners
			unregiterEventListeners: function() {
				var moduleEventListeners = _eventDispatcher.find({'subscriber': _identity});
				for (var i=0; i<moduleEventListeners.length; i++) {
					_eventDispatcher.remove(moduleEventListeners[i]);
				}
			}
		};
	};
	return Module;
});
