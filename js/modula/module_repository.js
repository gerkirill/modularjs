define(["modula/repository", "modula/event_dispatcher", "jquery"], function(Repository, EventDispatcher, jQuery){
	var ModuleRepository = function(){

		var _modules = Repository();
		var _eventDispatcher = EventDispatcher();
		var _orphanWidgets = Repository();

		function _add(module) {
			// add module to the _modules repository, index by identifier
			module.setEventDispatcher(_eventDispatcher);
			module.registerEventListeners();
			_modules.add(module, {identity: module.getIdentity()});
			// register module event listeners with _eventDispatcher, index by:
			// 1. owning module identifier, 2. targeted event, 3. event owner module identifier (optional)
		}

		function _loadModule(moduleIdentity) {
			require(["modules/"+moduleIdentity], function(module){
				_add(module);
				var moduleOrphanWidgets = _orphanWidgets.find({'module':moduleIdentity});
				for (var i=0; i<moduleOrphanWidgets.length; i++) {
					module.addWidget(moduleOrphanWidgets[i]);
					moduleOrphanWidgets[i].$.data('module-initialized', 'true');
					_orphanWidgets.remove(moduleOrphanWidgets[i]);
				}
			});
		}

		return {
			add: function(module) {
				_add(module);
			},
			remove: function(module) {
				// unregister event listeners from _eventDispatcher
				module.unregisterEventListeners(module);
				// remove from _modules repository
				_modules.remove(module);
			},
			initWidgets: function() {
				var modulesToLoad = [];
				// TODO: works bad if more than 1 widget which belong to the same medule - loading process may be 
				// initialized twice or more times
				jQuery('.m-module').each(function() {
					var widget = this;
					var $widget = jQuery(widget);
					if ($widget.data('module-initialized') === 'true') return;
					var moduleIdentity = $widget.data('module');
					if (!moduleIdentity) throw "Html element has 'm-module' class but does not have 'data-module' attribute";
					widget.$ = $widget;
					var matchingModules = _modules.find({identity: moduleIdentity});
					
					// TODO: exception if not found
					if (matchingModules.length < 1) {
						_orphanWidgets.add(widget, {'module': moduleIdentity});
						if (-1 === modulesToLoad.indexOf(moduleIdentity)) {
							modulesToLoad.push(moduleIdentity);
						}
					}
					else {
						var module = matchingModules[0];
						module.addWidget(widget);
					}	
				});
				while (modulesToLoad.length >= 1) {
					_loadModule(modulesToLoad.shift());
				}
			}
		};
	};
	return ModuleRepository;
});