define([
	// The dojo/dom module is required by this module, so it goes
	// in this list of dependencies.
	"jquery/jquery"
], function(j){
	// Once all modules in the dependency list have loaded, this
	// function is called to define the demo/myModule module.
	//
	// The dojo/dom module is passed as the first argument to this
	// function; additional modules in the dependency list would be
	// passed in as subsequent arguments.
 
	// This returned object becomes the defined value of this module
	return {
		sayHello: function(){
			console.log('hello!');
            console.log(j);
		}
	};
});