define(["modula/module"], function(Module){
	var module1 = Module();
	module1.define('module1', function(module) {
		// can I use this here to access module? bad idea for async. things
		function _textChanged(searchText) {
			console.log('search_text_changed - triggered');
			module.trigger('search_text_changed', {text: searchText});
		}
		var _handlesCount = 0;
		return {
			init: function(widget) {
				widget.$.on('keyup', '.searchField', function(){
					_textChanged(this.value);
				});
			},
			listeners: {
				'search_text_change_handled': function(event, widget) {
					var $counter = widget.$.find('.handlesCount');
					$counter.html(++_handlesCount);
				}
			}
		}
	});
	return module1;
});