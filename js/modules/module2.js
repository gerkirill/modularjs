define(["modula/module"], function(Module){
	var module2 = Module();
	module2.define('module2', function(module){
		return {
			listeners: {
				'search_text_changed' : function(event, widget) {
					widget.$.find('.searchText').html(event.text);
					module.trigger('search_text_change_handled', {});
					console.log('search_text_changed - handled: '+event.text);
				}
			}
		}
	});
	return module2;
});