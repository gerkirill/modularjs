/*
* Stores js modules,
* Set module up while adding it (sets event dispatcher and registers module event listeners)
* Unregisters module event listeners upon removal (can js module be removed? upon js module reload I guess)
* Allows to regiter html widgets awailable at the page in the correponding js modules
*/
var ModuleRepository = function(Repository, EventDispatcher, jQuery, Module){

	var _modules = Repository();
	var _eventDispatcher = EventDispatcher(Repository);

	/*
	* Initializes html widget if not yet initialized
	* @param widget Html element - widget root element (container) - should have data-module attribute
	* containing module class.
	*/
	function _initWidget(widget) {
		// html widget wrapped with jQuery
		var $widget = jQuery(widget);
		// check if the widget is already initializes
		if ($widget.data('module-initialized') === 'true') return;
		var moduleIdentity = $widget.data('module');
		if (undefined == moduleIdentity) {
			throw 'There is a widget with class m-module at the page, but without data-module attribute.';
		}
		if ('' == moduleIdentity) {
			throw 'There is a widget with class m-module at the page with empty data-module attribute.';
		}
		var matchingModules = _modules.find({identity: moduleIdentity});
		if (0 === matchingModules.length) {
			throw 'Can not find module "'+moduleIdentity+'", but some widget requires it.';
		}
		var module = matchingModules[0];
		//widget.$ = $widget;
		module.addWidget(widget);
		$widget.data('module-initialized', 'true');
	}

	function _add(module) {
		// add module to the _modules repository, index by identifier
		module.setEventDispatcher(_eventDispatcher);
		module.registerEventListeners();
		_modules.add(module, {identity: module.getIdentity()});
		// register module event listeners with _eventDispatcher, index by:
		// 1. owning module identifier, 2. targeted event, 3. event owner module identifier (optional)
	}

	return {
		/*
		* High-level function which defines module and adds it to module repository at once
		*/
		defineModule: function(identity, definingFunction) {
			var module = Module(Repository, jQuery);
			module.define(identity, definingFunction);
			_add(module);
		},

		/*
		* Adds module to repository and registers its event listeners with event dispatcher
		*/
		add: function(module) {
			return _add(module);
		},

		/*
		* Removes modules from repository and unregisters its event listeners from event dispatcher
		*/
		remove: function(module) {
			// unregister event listeners from _eventDispatcher
			module.unregisterEventListeners(module);
			// remove from _modules repository
			_modules.remove(module);
		},

		/*
		* Initializes html widgets at the page (html elements with m-module class and data-module attribute).
		* Already initalized widgets are just skipped
		*/
		initWidgets: function() {
			jQuery('.m-module').each(function() {
				_initWidget(this);
			});
		}
	};
};