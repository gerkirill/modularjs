var Repository = function(){

	// elements added to the repository
	var _elements = [];
	// 'field' => 'value' => [element IDS]
	var _indexes = {};

	function _addIndex (index, indexValue, elementKey) {
		if (typeof _indexes[index] === 'undefined') {
			_indexes[index] = {};
		}
		if (typeof _indexes[index][indexValue] === 'undefined') {
			_indexes[index][indexValue] = [];
		}
		_indexes[index][indexValue].push(elementKey);
	}

	// indexCount - object: key => matched indexes count
	// matchingElementKeys - array of integers
	function _updateMatchingIndexesCount(indexCount, matchingElementKeys) {
		for (var i=0; i<matchingElementKeys.length; i++) {
			var key = matchingElementKeys[i];
			if (typeof indexCount[key] === 'undefined') {
				indexCount[key] = 1;
			} else {
				indexCount[key]++;
			}
		}
		return indexCount;
	}

	// object ids key-id, value- their count
	function _findIdsMatchedXTimes(ids, matchesRequired) {
		var resultIds = [];
		for (id in ids) {
			var matchedTimes = ids[id];
			if (matchedTimes === matchesRequired) {
				resultIds.push(id);
			}
		}
		return resultIds;
	}

	function _findElementsWhichExistByIds(elementIds) {
		var elements = [];
		for (var i=0; i<elementIds.length; i++) {
			var elementId = elementIds[i];
			if (typeof _elements[elementId] !== 'undefined') {
				elements.push(_elements[elementId]);
			}
		}
		return elements;
	}

	return {
		/**
		 * adds element to the repository, also adds it to all the indexes for the search
		 */
		add: function(element, indexes) {
			var newLength = _elements.push(element);
			var elementId = newLength - 1;
			for (var index in indexes) {
				var indexValue = indexes[index];
				_addIndex(index, indexValue, elementId);
			}
		},

		/*
		 * finds element in the repository by one or multiple key=>value pairs
		 */
		find: function(criteria) {
			// array of elements from the repository which match all the key->value pairs
			var matchingElements = [];
			// key - element index, value - how much criteria element with this this id matches
			var indexesMatchCount = {};
			// number of indexes mentioned in criteria
			var numberOfIndexes = 0;
			var matchingElementIds = [];
			// find matching element ids by each key, find common ones, existing in _elemets array
			for (var index in criteria) {
				numberOfIndexes++;
				var indexValue = criteria[index];
				if (typeof _indexes[index] === 'undefined') continue;
				if (typeof _indexes[index][indexValue] === 'undefined') continue;
				var idsMatchingIndex = _indexes[index][indexValue];
				indexesMatchCount = _updateMatchingIndexesCount(indexesMatchCount, idsMatchingIndex);
			}
			matchingElementIds = _findIdsMatchedXTimes(indexesMatchCount, numberOfIndexes);
			// now add elements with ids in matchingElementIds, which exist in _elements
			matchingElements = _findElementsWhichExistByIds(matchingElementIds);
			return matchingElements;
		},

		remove: function(element) {
			for (var i=0; i<_elements.length; i++) {
				if (_elements[i] === element) {
					delete _elements[i];
					return;
				}
			}
		},

		getAll: function() {
			var elements = [];
			// skip removed elements
			for (var i=0; i<_elements.length; i++) {
				if (typeof _elements[i] !== 'undefined') {
					elements.push(_elements[i]);
				}
			}
			return elements;
		},

		removeAll: function() {
			_elements = [];
			_indexes = {};
		}
	};
};