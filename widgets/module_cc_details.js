window.modlr.defineModule('cc_details', function(module){
// module private parts
var $ = module.$;
function _showCcDetails(widget, ccId) {
	// if widget already shows details of proper CC - return
	if ( widget.$.data('ccId') == ccId ) return;
	// trigger event informing loading is started
	module.trigger('cc_details_load_started', {id: ccId});
	// load CC details over ajax 
	$.get('widget_cc_details.html', {id: ccId}, function(newWidgetContent){
		_ccDetailsLoaded(widget, newWidgetContent, ccId);
	});
}

function _ccDetailsLoaded(widget, newWidgetContent, ccId) {
	// emulate ajax delay
	setTimeout(function(){
		// replace old widget with new one
		var newWidget = $(newWidgetContent).replaceAll(widget);
		module.removeWidget(widget);
		module.addWidget(newWidget);
		newWidget.$.data('ccId', ccId);
		newWidget.$.find('.data-cc_id').html(ccId);
		// trigger event about loading done
		module.trigger('cc_details_load_finished', {id: ccId});
	}, 1500);
}

// module definition
	return {
		listeners: {
			'cc_clicked': function(event, widget) {
				var ccId = event.id;
				_showCcDetails(widget, ccId);
			}
		}
	}
//module end
});