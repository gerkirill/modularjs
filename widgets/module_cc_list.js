window.modlr.defineModule('cc_list', function(module){
// module private parts
var $ = module.$;
var _loadStarted = function(event, widget) {
	var ccId = event.id;
	widget.$.find('.data-indicator_'+ccId).show();
	console.log('progress started');
};

var _loadFinished = function(event, widget) {
	var ccId = event.id;
	widget.$.find('.data-indicator_'+ccId).hide();
	console.log('progress finished');
};

// module definition
	return {
		init: function(widget) {
			widget.$.on('click', '.data-item_link', function() {
				var ccId = $(this).data('id');
				console.log('clicked card #' + ccId);
				module.trigger('cc_clicked', {id: ccId});
				return false;
			});
		},
		listeners: {
			'cc_details_load_started': _loadStarted,
			'cc_details_load_finished': _loadFinished
		}
	}
//module end
});