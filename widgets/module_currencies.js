window.modlr.defineModule('currencies', function(module){
// module private parts
var $ = module.$;

function _loadWidgetIfNotYet(widget) {
	var $widget = widget.$;
	if (! $widget.data('loaded')) {
		_loadWidget($widget);
	}
}

function _loadWidget($widget) {
	setTimeout(function(){
		var url = $widget.data('url');
		$.get(url, function(widgetContent){
			var newWidget = $(widgetContent).replaceAll($widget);
			$(newWidget).data('loaded', 1);
			$(newWidget).data('url', url);
			module.removeWidget($widget[0]);
			module.addWidget(newWidget);
		});
	}, 2000);
}

// module definition
	return {
		init: function(widget) {
			_loadWidgetIfNotYet(widget);
		}
	}
//module end
});